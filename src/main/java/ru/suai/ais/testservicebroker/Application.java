package ru.suai.ais.testservicebroker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * Created by rost on 13.11.17.
 * Simple main Class
 *
 * Need to run the project.
 * <br>If you want see about http request:
 * {@link ru.suai.ais.testservicebroker.controller.ServiceSenderController}
 * <br>if you want see about service configuration:
 * {@link ru.suai.ais.testservicebroker.services.SettingsController} and
 * {@link ru.suai.ais.testservicebroker.repository.ConfigRepository}
 * <br>if you want see about service binding and service create:
 * {@link ru.suai.ais.testservicebroker.services.BeanServiceInstanceBindingService}
 *
 */

@Configuration
@SpringBootApplication
@ComponentScan(basePackages = { "ru.suai.ais.testservicebroker",
        "org.springframework.cloud.servicebroker.controller",
        "ru.suai.ais.testservicebroker.repository"
})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
