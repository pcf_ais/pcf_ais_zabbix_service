package ru.suai.ais.testservicebroker.services;

import org.apache.commons.lang3.RandomStringUtils;
import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.suai.ais.container.HostCreationResponse;
import ru.suai.ais.testservicebroker.controller.container.ZabbixInfo;
import ru.suai.ais.testservicebroker.exception.IllegalCreateHostException;
import ru.suai.ais.testservicebroker.model.ServiceInstanceCollector;
import ru.suai.ais.testservicebroker.repository.ServiceInstanceRepository;
import ru.suai.ais.zabbix.NewHostParams;
import ru.suai.ais.zabbix.User;
import ru.suai.ais.zabbix.Zabbix;


import java.io.IOException;
import java.util.List;

/**
 * Created by rost on 13.11.17.
 * need to manage some information about storage
 *
 */
@Service
public class MyService {
    /**
     * container for services
     *
     * key its ID binding service service
     *
     * value its the name of service
     * */
    @Autowired
    ServiceInstanceRepository serviceInstanceRepository;

    /**
     * Information about Zabbix server
     * */
    @Autowired
    ZabbixInfo zabbixInfo;

    /**
     * This is recomended method to find binding service.
     *
     * @param key  it's appID
     *
     * @return the first searched value
     * */
    public Object getServiceBinding(String key){
        List<ServiceInstanceCollector> lsic = serviceInstanceRepository.findByAppID(key);
        if(lsic == null)
            return null;
        else if(lsic.size() == 0)
            return null;
        return lsic.get(0);
    }

    /**
     * Not working, now, we will do this code<br>
     *
     * <b>TODO</b> make this stuff
     * */
    public Object eraseServiceBinding(String key){
        //serviceInstanceRepository.delete(serviceInstanceRepository.findByAppID(key).get(0));
        return null;
    }

    /**
     * This method is returned app-id.
     *
     * Register Host and create user for zabbix.
     *
     * @param zabbix Zabbix Class, which was created by ilia
     *
     * @param user always same
     *
     * @return app ID. In the future it's parametr need to manage zabbix server. All work I
     * */
    public String  addServiceBinding(Zabbix zabbix, User user, String pswd){
        String result = RandomStringUtils.randomAlphanumeric(25);
        if(pswd != null)
            result = pswd + "";
        NewHostParams newHostParams = new NewHostParams();
        newHostParams.host = result; //generate new statement

        newHostParams.addGroup("6");
        newHostParams.addInterface(1, 1, 1, "127.0.0.1", "",
                zabbixInfo.getPort());
        HostCreationResponse hostCreationResponse = null;
        try {
            hostCreationResponse = zabbix.createHost(user, newHostParams);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalCreateHostException(e.getMessage());
        }

        long idHost = hostCreationResponse.getHostId();
        ServiceInstanceCollector serviceInstanceCollector = new ServiceInstanceCollector(idHost, result);
        serviceInstanceCollector.setServiceinstanceID(idHost);
        serviceInstanceCollector.setAppID(result);
        //TODO generate login and password to zabbix and service broker

        serviceInstanceRepository.save(serviceInstanceCollector);
        return result;
    }


    /**@deprecated
     * check app-id in storage, its not safe, use getServiceBinding<br>
     *
     * <b>TODO</b> collect login and password for zabbix something else
     *
     * @param key app ID
     *
     * @param password password to acess to application service broker
     *
     *
     * */
    public boolean consistPassword(String password, String key){
        boolean result = false;


        return result;
    }

    /**
     * Returned info<br>
     *
     *
     * @param key appID
     *
     * @return null, because it's not done yet
     * */
    public Object getServiceInstance(String key){

        return null;
    }
    /**
     * @param key app ID
     *
     * @throws RuntimeException if service already exist
     *
     * */
    public Object eraseServiceInstance(String key){
        return null;
    }

    public void  addServiceInstance(String key, Object serviceInstanceBinding){
        if(getServiceInstance(key) != null)
            throw new RuntimeException("Service already exist");

    }

    public Object getStorage(){
        return this.serviceInstanceRepository;
    }

}
