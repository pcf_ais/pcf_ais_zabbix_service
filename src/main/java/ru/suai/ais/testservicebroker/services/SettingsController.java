package ru.suai.ais.testservicebroker.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.servicebroker.model.Catalog;
import org.springframework.cloud.servicebroker.model.ServiceDefinition;
import org.springframework.cloud.servicebroker.service.BeanCatalogService;
import org.springframework.cloud.servicebroker.service.CatalogService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import ru.suai.ais.testservicebroker.controller.Destroy;
import ru.suai.ais.testservicebroker.controller.container.ZabbixInfo;
import ru.suai.ais.testservicebroker.model.plans.PlanFactory;
import ru.suai.ais.zabbix.User;
import ru.suai.ais.zabbix.Zabbix;


import java.io.IOException;
import java.util.*;

/**
 * Created by rost on 13.11.17.
 * It's necessary to initialize some values<br>
 * logging by slf4j
 *
 *
 *
 *
 *
 */
@Slf4j
@Controller
public class SettingsController {

    MyService myService = new MyService();
    @Value("${#{environment.ZBX_WEB_INTERFACE}:zabbix-web-auto.apps.testpcf.org}")
    String url;
     /**
      * default Zabbix port
      * <br>
      * */
    String port = "10051";

    /**
     * It's Zabbix api. To learn more
     * @see Zabbix
     * */
    Zabbix zabbix = null;
    User userZabbix;

    @Bean
    public Catalog getCatalog() {
        return new Catalog(Collections.singletonList(
                new ServiceDefinition("example-service-broker-1",
                        "example-broker-zabbix",
                        "This is test example service broker for debug",
                        true, false,
                        Arrays.asList(
                                PlanFactory.getPlan()),
                        Arrays.asList("zabbix-cf-broker"),
                        getMetadata(),
                        null,
                        null
                        )
        ));
    }

    /**
     * Need for Service Broker
     * @return BeanCatalogService
     * @see BeanCatalogService
     * */
    @Bean
    public CatalogService getCatalogService(Catalog catalog){
        return new BeanCatalogService(catalog);
    }

    /**
     * return default value
     * @see MyService
     * @see SettingsController#myService
     * */
    @Bean
    public  MyService getMyService(){return  myService;}

    @Bean
    public ZabbixInfo zabbixInfo(){
        Map<String, String>env = System.getenv();
        ZabbixInfo zabbixInfo = new ZabbixInfo();

        zabbixInfo.setUrl(this.url);
        if(env.get("ZBX_WEB_INTERFACE") != null)
            zabbixInfo.setUrl(env.get("ZBX_WEB_INTERFACE"));
        if(env.get("ZBX_SERVER_HOST") != null)
            zabbixInfo.setSendURL(env.get("ZBX_SERVER_HOST"));
        if(env.get("ZBX_SERVER_PORT") != null)
            zabbixInfo.setPort(port = env.get("ZBX_SERVER_PORT"));
        else
            zabbixInfo.setPort(port);
        return zabbixInfo;
    }

    /**
     * Get managed user from System Env.
     *
     * for more information
     * @see User
     * */
    @Bean
    User getUser() throws IOException {
        if(userZabbix == null){
            Map<String, String>env = System.getenv();
            String username = "Admin";
            String password = "zabbix";
            if(env.get("ZBX_USER_ROOT_NAME") != null)
                username = env.get("ZBX_USER_ROOT_NAME");
            if(env.get("ZBX_USER_ROOT_PASSWORD") != null)
                password = env.get("ZBX_USER_ROOT_PASSWORD");
            int userId = 1;
            try {
                userZabbix = zabbix.login(username, password, userId);
            }catch (Exception e){
                e.printStackTrace();
                log.error(e.toString());
            }
            return userZabbix;
        }else return userZabbix;
    }

    /**
     * get zabbix interface
     *
     * @return Zabbix class
     * @see Zabbix
     * */
    @Bean Zabbix getZabbix(){
        if(zabbix == null){
            Map<String, String>env = System.getenv();
            if(env.get("ZBX_WEB_INTERFACE") != null)
                zabbix = new Zabbix(env.get("ZBX_WEB_INTERFACE"));
            else
                zabbix = new Zabbix(this.url);
        }
        return zabbix;
    }

    /**
     * This is need for PCF
     *
     * TODO add support URL
     * */
    private Map<String, Object>getMetadata(){
        Map<String,Object> planMetadata = new HashMap<>();
        planMetadata.put("displayName", "zabbix-cf-broker");
        planMetadata.put("longDescription", "Created by Rost. \nThis is test service broker for a manage zabbix service." +
                "This is front-end point to communicate with zabbix server");
        planMetadata.put("providerDisplayName", "suai");
        planMetadata.put("imageUrl", "https://ma.ttias.be/wp-content/uploads/2017/03/zabbix_logo.png");
        planMetadata.put("supportUrl", "none");
        return planMetadata;
    }

    /*
    * need to close connection with Zabbix Server
    * */
    @Bean
    DisposableBean getDisposableBean(){
        return new Destroy(zabbix, userZabbix);
    }
}
