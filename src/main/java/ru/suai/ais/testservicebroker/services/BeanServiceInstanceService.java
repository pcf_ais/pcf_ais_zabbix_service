package ru.suai.ais.testservicebroker.services;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.servicebroker.model.*;
import org.springframework.cloud.servicebroker.service.ServiceInstanceService;
import org.springframework.stereotype.Service;

import ru.suai.ais.zabbix.*;
import ru.suai.ais.zabbix.exception.IllegalCreateUserException;

import java.io.IOException;

/**
 * Created by rost on 13.11.17.
 * See documentation service broker PCF
 * In this I create new Host.
 * @see BeanServiceInstanceBindingService
 */
@Slf4j
@Service
public class BeanServiceInstanceService implements ServiceInstanceService {

    @Autowired
    MyService myService;

    @Autowired
    Zabbix zabbix;

    @Autowired
    User user;

    public BeanServiceInstanceService(MyService myService){
        this.myService = myService;
        ;
    }

/**
 * TODO you have to do it
 * */
    @Override
    public CreateServiceInstanceResponse createServiceInstance(CreateServiceInstanceRequest createServiceInstanceRequest) {
        CreateServiceInstanceResponse result = new CreateServiceInstanceResponse();
        /*NewUserGroupParams hostGroupParams = new NewUserGroupParams();
        hostGroupParams.setName(createServiceInstanceRequest.getSpaceGuid() + "-group");
        JsonNode addAnswer = null;
        try {
            addAnswer = (JsonNode)zabbix.createUserGroup(user, hostGroupParams);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalCreateUserException("Can't create  new user group");
        }

        NewUserParams newUserParams = new NewUserParams();
        newUserParams.alias = createServiceInstanceRequest.getSpaceGuid();
        newUserParams.name = createServiceInstanceRequest.getOrganizationGuid();
        newUserParams.surname = createServiceInstanceRequest.getPlanId();
        newUserParams.setPassword("123456");//*/
        return result;
    }

    @Override
    public GetLastServiceOperationResponse getLastOperation(GetLastServiceOperationRequest getLastServiceOperationRequest) {
        return new GetLastServiceOperationResponse().withOperationState(OperationState.SUCCEEDED);
    }

    @Override
    public DeleteServiceInstanceResponse deleteServiceInstance(DeleteServiceInstanceRequest deleteServiceInstanceRequest) {

        return new DeleteServiceInstanceResponse();
    }

    @Override
    public UpdateServiceInstanceResponse updateServiceInstance(UpdateServiceInstanceRequest updateServiceInstanceRequest) {
        return null;
    }
}
