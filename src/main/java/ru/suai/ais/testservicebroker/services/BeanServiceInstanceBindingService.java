package ru.suai.ais.testservicebroker.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.app.ApplicationInstanceInfo;
import org.springframework.cloud.servicebroker.model.*;
import org.springframework.cloud.servicebroker.service.ServiceInstanceBindingService;
import org.springframework.stereotype.Service;
import ru.suai.ais.testservicebroker.controller.container.ZabbixInfo;
import ru.suai.ais.testservicebroker.model.ServiceInstanceCollector;
import ru.suai.ais.zabbix.User;
import ru.suai.ais.zabbix.Zabbix;

import java.util.*;

/**
 * Created by rost on 13.11.17.
 * See documentation Service broker PCF<br>
 * When application bind to service Broker, Host is created.
 *
 * @see <a href="https://github.com/openservicebrokerapi/servicebroker/blob/v2.12/spec.md"> Service broker API</a>
 * @see <a href="https://github.com/cloudfoundry-community/spring-boot-cf-service-broker">Service broker for GiThub</a>
 */
@Slf4j
@Service
public class BeanServiceInstanceBindingService implements ServiceInstanceBindingService {

    @Autowired(required = false)
    ApplicationInstanceInfo applicationInstanceInfo;
    @Autowired(required = false)
    MyService myService;

    @Autowired
    Zabbix zabbix;
    @Autowired
    User user;
    @Autowired
    ZabbixInfo zabbixInfo;



    @Autowired
    public BeanServiceInstanceBindingService(MyService myService){
        this.myService = myService;
    }

    @Override
    public CreateServiceInstanceBindingResponse createServiceInstanceBinding(
            CreateServiceInstanceBindingRequest createServiceInstanceBindingRequest) {

        String bindingId = createServiceInstanceBindingRequest.getBindingId();
        String serviceInstanceId = createServiceInstanceBindingRequest.getServiceInstanceId();

        String password = myService.addServiceBinding(zabbix, user,
                createServiceInstanceBindingRequest.getBindingId());

        log.debug("Create service instance with ID:" +  bindingId);

        Map<String, Object>credentials = getCredentials();
        credentials.put("app_id", password);
        ServiceInstanceCollector sic = (ServiceInstanceCollector)myService.getServiceBinding(password);
        credentials.put("id_host", sic.getServiceinstanceID());

        ServiceInstanceBinding binding = new ServiceInstanceBinding(bindingId, serviceInstanceId,
                null, null, createServiceInstanceBindingRequest.getBoundAppGuid());

        //*/
        return new CreateServiceInstanceAppBindingResponse().withCredentials(credentials);
    }

    @Override
    public void deleteServiceInstanceBinding(DeleteServiceInstanceBindingRequest deleteServiceInstanceBindingRequest) {
        String bindingID = deleteServiceInstanceBindingRequest.getBindingId();
        myService.eraseServiceBinding(deleteServiceInstanceBindingRequest.getBindingId());
    }

    /**
     * Generate credentials:<br>
     * <pre>
     *     "credentials": {
                "urls": [
                    "appsservicebroker.apps.testpcf.org"
                    ],
                "app_id": "ed8daf12-2c98-4768-8277-845bbe59993b",
                "id_host": 10282
            },
     * </pre>
     *
     * */
    Map<String, Object> getCredentials(){
        HashMap<String, Object>result = new LinkedHashMap<String, Object>();
        Map<String, Object>properties = this.applicationInstanceInfo.getProperties();
        result.put("urls", properties.get("application_uris"));
        result.put("port", zabbixInfo.getPort());
        //result.put("uris", listUris);
        return result;
    }
}
