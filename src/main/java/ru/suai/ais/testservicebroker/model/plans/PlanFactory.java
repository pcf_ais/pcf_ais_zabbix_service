package ru.suai.ais.testservicebroker.model.plans;

import org.springframework.cloud.servicebroker.model.Plan;

/**
 * Created by rost on 19.11.17.
 * need to create simple plan.<br>
 *
 * Now it's return plan apout client
 *
 *
 */
public class PlanFactory {
    public static final Plan[]plans = {
            new Plan("my-plan-test",
                    "client",
                    "This is second plan. This plan requiere client application to throw information.")
    };

    public static Plan[]getPlan(){ return plans;}
}
