package ru.suai.ais.testservicebroker.model;




import javax.persistence.*;
import java.io.Serializable;


/**
 * Created by rost on 24.11.17.
 * It's need to collect all information about service instance
 *
 * I want to collect and users too but I have no opportunity to manage users in zabbix.
 */
@Entity
public class ServiceInstanceCollector  {

    /**
     * It's value collect host id from zabbix server
     * */
    @Id
    private Long serviceinstanceID;

    /**
     * It's collect uniqual string
     * */
    private String appID;


    ServiceInstanceCollector(){}


    /**
     * @param serviceinstanceID
     *          it's some id for service instance, must be individual
     * */
    public ServiceInstanceCollector(Long serviceinstanceID, String appID){
        this.serviceinstanceID = serviceinstanceID;
        this.setAppID(appID);
    }

    public void setAppID(String appID){this.appID = appID;}

    public void setServiceinstanceID(Long serviceinstanceID){this.serviceinstanceID = serviceinstanceID;}

    public Long getServiceinstanceID(){return  serviceinstanceID;}

    public String getAppID(){return appID;}


    /**
     * It's need to find object for only serviceinstanceID
     *
     * @return hash serviceinstanceID
     * */
    public int hashCode(){
        return serviceinstanceID.hashCode();
    }

    public boolean equals(Object obj){
        if(obj instanceof ServiceInstanceCollector){
            return getServiceinstanceID().equals(((ServiceInstanceCollector)obj).getServiceinstanceID());
        }else return false;
    }

    public String toString(){
        return String.format("ServiceInstanceCollector=[serviceinstanceID=%d, appID='%s']",
                serviceinstanceID.intValue(),
                this.getAppID());
    }


}
