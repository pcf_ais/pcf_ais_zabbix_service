package ru.suai.ais.testservicebroker.exception;

/**
 * Created by rost on 29.11.17.
 */
public class IllegalCreateHostException extends RuntimeException {
    public IllegalCreateHostException(){
        super();
    }

    public IllegalCreateHostException(String str){
        super(str);
    }
}
