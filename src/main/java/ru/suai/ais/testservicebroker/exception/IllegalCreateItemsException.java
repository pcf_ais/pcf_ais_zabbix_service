package ru.suai.ais.testservicebroker.exception;

/**
 * Created by rost on 29.11.17.
 */
public class IllegalCreateItemsException extends RuntimeException {

    public IllegalCreateItemsException(){
        super();
    }

    public IllegalCreateItemsException(String str){
        super(str);
    }
}
