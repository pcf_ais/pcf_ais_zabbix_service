package ru.suai.ais.testservicebroker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.suai.ais.testservicebroker.model.ServiceInstanceCollector;

import java.util.List;

/**
 * Created by rost on 24.11.17.
 *
 * This is need to collect all information about services instances + save new user login and password for zabbix and
 * service broker<br>
 *
 * If you want learn more about containier
 * {@link ServiceInstanceCollector}
 */
public interface ServiceInstanceRepository extends JpaRepository<ServiceInstanceCollector, Long> {
    /**
     * find by appID
     *
     * @param appID this is uniqual key to search some information
     *
     *              @return List of finding values
     *
     * */
    List<ServiceInstanceCollector> findByAppID(String appID);
}
