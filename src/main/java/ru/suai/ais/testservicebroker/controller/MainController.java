package ru.suai.ais.testservicebroker.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rost on 13.11.17.
 * Nothing do
 */

@RestController
public class MainController {
    @RequestMapping("/")
    public String getIndex(){
        return "Hello world";
    }
}
