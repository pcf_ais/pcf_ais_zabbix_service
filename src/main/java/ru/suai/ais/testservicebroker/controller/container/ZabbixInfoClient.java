package ru.suai.ais.testservicebroker.controller.container;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by rost on 23.11.17.
 * This class created for recieve/send information about Zabbix server NOT FOR Service Broker
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZabbixInfoClient {
    String url = "";
    String appId = "123";
    String idHost = "";
    public void setIdHost(String idHost){this.idHost = idHost;}
    public String getIdHost(){return  idHost;}
    String numPort;
    public void setNumPort(String numPort){this.numPort = numPort;}
    public String getNumPort(){return numPort;}
    List<Object>values = new LinkedList<Object>();

    public ZabbixInfoClient(){}

    public  void setUrl(String url){this.url = url;}
    public void setAppId(String appId){this.appId = appId;}
    public void setValues(List<Object>list){this.values = list;}



    public String  getUrl(){return url; }
    public String getAppId(){return appId;}
    public List<Object>getValues(){return values;}
}
