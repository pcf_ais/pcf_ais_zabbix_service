package ru.suai.ais.testservicebroker.controller;

import org.apache.commons.collections.FastTreeMap;
import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import ru.suai.ais.testservicebroker.controller.container.ZabbixInfo;
import ru.suai.ais.testservicebroker.controller.container.ZabbixInfoClient;
import ru.suai.ais.testservicebroker.exception.IllegalCreateHostException;
import ru.suai.ais.testservicebroker.exception.IllegalCreateItemsException;
import ru.suai.ais.testservicebroker.model.ServiceInstanceCollector;
import ru.suai.ais.testservicebroker.services.MyService;
import ru.suai.ais.zabbix.NewHostParams;
import ru.suai.ais.zabbix.NewItemParams;
import ru.suai.ais.zabbix.User;
import ru.suai.ais.zabbix.Zabbix;

import java.io.IOException;
import java.util.Map;
import java.util.Random;


/**
 * Created by rost on 17.11.17.<br>
 *Controller for http requests
 *
 * <b>TODO</b> delete some references, because it's note safe to use it.
 * <br>
 *
 * <b>TODO</b> add validator to validate params
 * <br>
 */

@RestController
public class ServiceSenderController {

    /**
     * This is custom class to manage all information about zabbix server
     * */
    @Autowired(required = false)
    MyService myService;

    /**
     * Info about Zabbix Server
     * */
    @Autowired
    ZabbixInfo zabbixInfo;

    /**
     * Zabbix API for manage server
     * */
    @Autowired
    Zabbix zabbix;

    /**
     * Info about Zabbix server
     * */
    @Autowired
    User user;

    /**
     * need to generate some value
     * */
    Random random = new Random(System.nanoTime());


    /**
     * need to testing
     *
     * @return some info
     * */
    @RequestMapping("tets/getinfofrombroker")
    Object getSomeInfoFromBroker(@RequestParam(value="id", required = false, defaultValue = "0") String id,
                                          @RequestParam(value="str", required = false, defaultValue = "bla-bla") String str){
        Map<Object, Object> info = new FastTreeMap();
        info.put("zabbix", zabbixInfo);
        info.put("zabbixUser", user);
        return info;
    }
    /**
     * Need to answer json request<br>
     *
     * Example http request:
     * @see <a href="https://appsservicebroker.apps.testpcf.org/suai/store/get?appID=NT1iR9CY1EOUAoYQ8INDzM2tF">
     *     https://appsservicebroker.apps.testpcf.org/suai/store/get?appID=NT1iR9CY1EOUAoYQ8INDzM2tF</a>
     * <br>Example json answer:
     * <pre>
     * {
     *      "url":"tcp.apps.testpcf.org",
     *      "appId":"NT1iR9CY1EOUAoYQ8INDzM2tF",
     *      "idHost":"10283",
     *      "numPort":"10051",
     *      "values":[]
     * }
     * </pre>
     *<br>This values need to connect some devices to zabbix service
     * <br>I want to save registered values in service broker, but now it's not working<br>
     *
     *
     * <br><b>TODO</b> Save or get information from zabbix about registered items
     *
     * @param appId id application service from zabbix. it's named "appID" in HTTP
     *
     * @return information from zabbix server(password, url)
     * @see ZabbixInfoClient
     * */
    @RequestMapping("/suai/store/get")
    ZabbixInfoClient getZabbixInfo(@RequestParam(value = "appID")String appId){
        ZabbixInfoClient zabbixInfoClient = new ZabbixInfoClient();
        zabbixInfoClient.setAppId(appId);
        ServiceInstanceCollector sic = (ServiceInstanceCollector)myService.getServiceBinding(appId);
        if(sic != null)
            zabbixInfoClient.setIdHost(sic.getServiceinstanceID().toString());

        zabbixInfoClient.setNumPort(zabbixInfo.getPort());

        zabbixInfoClient.setUrl(zabbixInfo.getSendURL());
        return zabbixInfoClient;
    }


    /**
     * This is need to create items in zabbix server<br>
     *
     * <br>answer json request:
     * <pre>
     * {
     *      "delay":30,
     *      "hostid":"10283",
     *      "interfaceid":"31",
     *      "key":"key-457927404",
     *      "name":"bla-bla",
     *      "type":2,
     *      "valueType":0
     * }
     * </pre>
     * Read zabix documentation more information about this values
     * @see <a href="https://www.zabbix.com/documentation/3.0/ru/manual/api/reference/item">documentation Zabbix API</a>
     * @param appId
     *              need to auth
     *
     * @param name  name your item
     *
     * */
    @RequestMapping("/suai/store/createItems")
    Object createItems(@RequestParam(value = "appID")String appId,
                       @RequestParam(value = "name")String name) throws IOException {
        ServiceInstanceCollector sic = (ServiceInstanceCollector) (myService.getServiceBinding(appId));
        if(!(sic  == null) && false)
            throw new RuntimeException("Invalid appId");
        //https://www.zabbix.com/documentation/3.0/ru/manual/api/reference/item
        int val = Math.abs(random.nextInt());
        NewItemParams newItemParams = new NewItemParams();
        newItemParams.setDelay(30);
        newItemParams.setName(name);
        newItemParams.setHostId(sic.getServiceinstanceID().toString());
        newItemParams.setType(2);
        newItemParams.setValueType(0);
        //TODO manage interfaceIDs
        newItemParams.setInterfaceId(
                Integer.toString(zabbix.getHostInterfaceID(newItemParams.getHostId(), user)));
        newItemParams.setKey("key-" + val);
        Object jsonNode = null;
        try {
            zabbix.createItem(user, newItemParams);
        }catch (IllegalCreateItemsException e){
            return "{ \"error\":" + e.getMessage() + "}";
        }

        return newItemParams;
    }

    /**
     * Custom create host
     *
     * I never used it and never debug it
     *
     * @param appId application ID
     *
     * @param ip ip host
     * */
    @RequestMapping("/suai/store/createHosts")
    Object createHosts(@RequestParam(value = "appID")String appId,
                       @RequestParam(value = "ip", required = false) String ip) throws IOException {
        NewHostParams newHostParams = new NewHostParams();
        newHostParams.host = "127.0.0.1";
        newHostParams.addGroup("12");
        newHostParams.addInterface(1, 1, 1, ip, "", "10050");
        try {
            return zabbix.createHost(user, newHostParams).toString();
        }catch (IllegalCreateHostException e){
            e.printStackTrace();
            return "{\"error\": " + e.getMessage() + "}";
        }
    }


    /**@deprecated
     * Register own application client
     *
     *
     * @param appId
     *              id application service from zabbix
     *
     *
     * @param values
     *              give JSON request
     *
     *
     * @return
     *          the name by host, null if don't not correct
     * @see String
     * */
    @RequestMapping("/suai/store/registerhost")
    String registerApp(@RequestParam(value = "appID")String appId, @RequestParam(value = "values")String values){
        if(!(myService.getServiceBinding(appId) == null))
            return "no password :(";

        return "Well Done";
    }

    /**@deprecated
     * This is test create host.
     *
     * I think to deprecated it, because it's not safe.
     * */
    @RequestMapping("/test/createHsot")
    Object testCreateHost(){

        return myService.addServiceBinding(zabbix, user, null);
    }


}
