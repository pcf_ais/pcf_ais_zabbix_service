package ru.suai.ais.testservicebroker.controller.container;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rost on 23.11.17.
 * I would like use this class to recieve info to zabbix server
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZabbixInfo extends ZabbixInfoClient {
    /**
     * Number of port to Zabbix server
     * */
    String port;
    public void setPort(String port){this.port = port;}
    public String getPort(){return port;}
    String sendURL;
    public void setSendURL(String url){this.sendURL = url;}
    public String getSendURL(){return sendURL;}
}
