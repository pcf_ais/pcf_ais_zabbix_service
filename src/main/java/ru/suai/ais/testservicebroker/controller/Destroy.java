package ru.suai.ais.testservicebroker.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import ru.suai.ais.zabbix.User;
import ru.suai.ais.zabbix.Zabbix;


import java.io.IOException;


/**
 * Created by rost on 07.12.17.
 *
 * Neeed to close connection from zabbix server
 */
public class Destroy implements DisposableBean{

    Zabbix zabbix;
    User userZabbix;
    Logger logger = LoggerFactory.getLogger(Destroy.class.getName());

    public Destroy(Zabbix zabbix, User userZabbix){
        this.userZabbix = userZabbix;
        this.zabbix = zabbix;
    }


    @Override
    public void destroy() throws Exception {
        try {
            if(userZabbix == null){
                logger.info("info about user is null, no logout");
                return;
            }

            zabbix.logout(userZabbix);
            logger.info("logout:" + userZabbix.getUsername());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
