#Intro
A Cloud Foundry Service Broker for Zabbix built using the [spring-boot-cf-service-broker](https://github.com/cloudfoundry-community/spring-boot-cf-service-broker).
##Design

The broker uses meta data in Zabbix API and naming conventions to maintain the state of the services it is brokering. 

Implements in this project: [Zabbix API](https://bitbucket.org/pcf_ais/zabbixapi/overview)

#Загрузка Service Broker

 **Service Broker** предоставляется в виде исходного кода как [maven](https://maven.apache.org/) приложение
Для того, чтобы загрузить приложение, необходимо: 

```
mvn package
cf push
cf create-service-broker example-broker-zabbix  user user-password  https://url-service-broker
cf enable-service-access example-broker-zabbix
cf create-service example-broker-zabbix client name-broker-instance
```
Для того, чтобы сделать из приложения сервис, необходимо, чтобы он реализовывал какое-то [API](https://github.com/spring-cloud/spring-cloud-cloudfoundry-service-broker), в данном API реализовано Spring boot security. По умолчанию, этот SB имеет логин: user и пароль: 0d4435a4-fecd-434a-b798-fc83d9ade3db 

Для того, чтобы Service Broker видел Zabbix-сервер, при запуске ему необходимо указать переменные среды. Можно указать вручную, можно с помощью manifiest.yml:
```
env:
  ZBX_SERVER_HOST: "tcp.apps.testpcf.org"
  ZBX_WEB_INTERFACE: "zabbix-web.apps.testpcf.org"
  ZBX_SERVER_PORT: "10051"
  ZBX_USER_ROOT_NAME: "Admin"
  ZBX_USER_ROOT_PASSWORD: "1234567"
```
ZBX_SERVER_HOST – Имя tcp хоста, на который будут отправляться данные.


ZBX_WEB_INTERFACE – Имя хоста, на котором живёт Веб-интерфейс для визуализации данных и настройки

ZBX_SERVER_PORT – Имя порта на который будут отправляться данные

ZBX_USER_ROOT_NAME – логин админа суперпользователя

ZBX_USER_ROOT_PASSWORD – пароль админа суперпользователя.

Суперпользователь нужен для конфигурирования Zabbix-сервера.


**Ссылки**

- ru.suai.ais.testservicebroker.controller.ServiceSenderController -- 
Контроллер для конфигурирования Zabbix-сервера
- ru.suai.ais.testservicebroker.services.SettingsController -- Конфигурирование 
ServiceBroker